package com.example.user.adminpanel.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.adminpanel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Admin extends AppCompatActivity {
    private Button logIn;
    private EditText emailField,passwordField;
    AppCompatCheckBox showAdminPassword;
    private FirebaseAuth mAuth;
    ProgressDialog progressDialog;
    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        emailField = findViewById(R.id.etAdminEmail);
        progressDialog=new ProgressDialog(this);
        passwordField = findViewById(R.id.etAdminPassword);
        mAuth = FirebaseAuth.getInstance();
        showAdminPassword = findViewById(R.id.checkboxPass);
        logIn = (Button) findViewById(R.id.btnLog);
        mAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if(firebaseAuth.getCurrentUser()!=null){
                    startActivity(new Intent(Admin.this,Admin_activity.class));
                    FirebaseAuth.getInstance().signOut();

                }
            }
        };
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLogIn();
            }
        });
        showAdminPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    passwordField.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                else {
                    passwordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    private void startLogIn() {
        String email= emailField.getText().toString();
        String password= passwordField.getText().toString();
        if(TextUtils.isEmpty(email)||TextUtils.isEmpty(password)){
            Toast.makeText(Admin.this,"Fields are empty",Toast.LENGTH_LONG).show();

        }else {
            progressDialog.setMessage("Please wait ");
            progressDialog.show();
            mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()){
                        Toast.makeText(Admin.this,"Sign in problem",Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();


                }
            });

        }
    }


}





