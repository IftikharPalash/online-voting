package com.example.user.adminpanel.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.Class.UserValidation;
import com.example.user.adminpanel.Class.VoterImportClass;
import com.example.user.adminpanel.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import static com.example.user.adminpanel.Class.UserValidation.validate;

public class LogIn_activity extends AppCompatActivity {
    ProgressDialog progressDialog;
    EditText emailUser,passwordUser;
    Button userLogIn;
   // Button btnLogIn;

    AppCompatCheckBox showUserPassword;
    TextView textAdmin;
    Query electionRef;
    DatabaseReference electionReference;

    DatabaseReference voterReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_activity);
        showUserPassword = findViewById(R.id.cbUserPass);
        emailUser=findViewById(R.id.etUserEmail);
        textAdmin=findViewById(R.id.adminText);
        progressDialog=new ProgressDialog(this);
        passwordUser=findViewById(R.id.etUserPassword);
       // btnLogIn = (Button) findViewById(R.id.btnLogIn);
        userLogIn=(Button)findViewById(R.id.btnUser) ;
        //voterReference = FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Voter");
        electionReference = FirebaseDatabase.getInstance().getReference();
        electionRef = electionReference.child("Election").limitToLast(1);




            showUserPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    passwordUser.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                else {
                    passwordUser.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        userLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validateInput()) {
                    initVoterData();
                }




            }
        });

        textAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Admin();

            }

        });


    }

//    private void UserLog() {
//        String email=emailUser.getText().toString();
//        String password=passwordUser.getText().toString();
//        if(email.equals("")|| password.equals("")){
//            Toast.makeText(this,"Please fill all fields",Toast.LENGTH_LONG).show();
//
//        }
//        else  {
//            VoterImportClass voterImportClass=new VoterImportClass(LogIn_activity.this);
//            voterImportClass.setEmail(email);
//            voterImportClass.setPassword(password);
//
//        }
////        if(TextUtils.isEmpty(email)){
////            Toast.makeText(LogIn_activity.this,"Email Fields is empty",Toast.LENGTH_LONG).show();
////            return;
////        }
////
////        if(TextUtils.isEmpty(password)){
////            Toast.makeText(LogIn_activity.this,"Password Fields is empty",Toast.LENGTH_LONG).show();
////            return;
////        }
////       // progressDialog.dismiss();
//
//      //  progressDialog.setMessage("Please wait ");
//      //  progressDialog.show();
////        logAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
////            @Override
////            public void onComplete(@NonNull Task<AuthResult> task) {
////                if(task.isSuccessful()){
////                    Toast.makeText(LogIn_activity.this,"Sign in Successfully",Toast.LENGTH_LONG).show();
////                }
////                else {
////                    Toast.makeText(LogIn_activity.this,"Sign in Problem",Toast.LENGTH_LONG).show();
////
////
////                }
////
////
////            }
////        });
//
//
//
//
//
//    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void initVoterData() {
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                AddElection addElection = null;
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election1: ", snapshot.getValue().toString());
                    addElection = snapshot.getValue(AddElection.class);
                    Log.v("ElectionStatus: ", addElection.getElectionStatus().toString());
                    election = snapshot.getKey().toString();

                }

                if ( addElection.getElectionStatus().equals("Enable")) {
                    voterReference = electionReference.child("Election").child(election).child("Voter");
                    Log.v("Election: ", voterReference.toString());
                    voterReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            VoterImportClass loggedInUser = new VoterImportClass();
                            loggedInUser = null;
                            boolean isUserExists = false;
                            Log.v("Voter: ", dataSnapshot.getChildren().toString());
                            String userId = emailUser.getText().toString();
                            String userPassword = passwordUser.getText().toString();
                            for (DataSnapshot voteInfoSnapshot : dataSnapshot.getChildren()){
                                VoterImportClass voterImportClass=voteInfoSnapshot.getValue(VoterImportClass.class);
                                Log.v("testVorer: ", voterImportClass.toString());

                                if (userId.equals(voterImportClass.getId())
                                        && userPassword.equals(voterImportClass.getPassword())) {
                                    loggedInUser = voterImportClass;
                                    break;
                                } else {
                                    loggedInUser = null;
                                }

                            }

                            updateUI(loggedInUser);

                        }


                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else {
                    Toast.makeText(LogIn_activity.this, "Try again later.", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
    }

    private boolean validateInput(){
        String id = emailUser.getText().toString().trim();
        String password = passwordUser.getText().toString().trim();
        if(id.isEmpty()){
            emailUser.setError("Empty Field");
            return false;
        }
        if(password.isEmpty()){
            passwordUser.setError("Empty Field");
            return false;
        }


        if (id.matches("[A-Z][A-Z][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9]")) {
            if (password.matches("[0-9]*")) {
                return true;
            } else {
                passwordUser.setError("Invalid Password");
            }

        } else {
            emailUser.setError("Invalid Email");
        }
        return false;
    }

    private void updateUI(VoterImportClass voterImportClass) {
        if(voterImportClass == null){
            Toast.makeText(LogIn_activity.this,"Email or password not match",Toast.LENGTH_LONG).show();
        }else {
            SharedPreferences sp = getSharedPreferences("storage", MODE_PRIVATE);
            sp.edit().putString("email", voterImportClass.getEmail()).putString("name",voterImportClass.getName())
                    .putString("id",voterImportClass.getId()).putString("password",voterImportClass.getPassword()).apply();

            Intent intent = new Intent(this, UserActivity.class);
            Toast.makeText(LogIn_activity.this,"Log in successFully",Toast.LENGTH_LONG).show();
            startActivity(intent);




        }

    }

    public void Admin() {
        Intent intent = new Intent(this, Admin.class);
        startActivity(intent);

    }



}