package com.example.user.adminpanel.Class;

import android.widget.EditText;

public class VoteAdd {
    String voterSerial,voterName,studentId,voterEmail,voteStatus,voterPassword;
    public VoteAdd(){

    }

    public VoteAdd(String voterSerial, String voterName, String studentId, String voterEmail, String voterPassword, String voteStatus) {
        this.voterSerial = voterSerial;
        this.voterName = voterName;
        this.studentId = studentId;
        this.voterEmail = voterEmail;
        this.voteStatus = voteStatus;
        this.voterPassword=voterPassword;
    }

    public VoteAdd(EditText voterSerial, EditText voterName, EditText studentId, EditText voterEmail, EditText voteStatus, EditText voterPassword) {
    }

    public String getVoterSerial() {
        return voterSerial;
    }

    public String getVoterName() {
        return voterName;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getVoterEmail() {
        return voterEmail;
    }

    public String getVoteStatus() {
        return voteStatus;
    }

     public String getVoterPassword() {
       return voterPassword;
     }
}
