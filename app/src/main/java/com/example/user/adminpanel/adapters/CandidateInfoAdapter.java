package com.example.user.adminpanel.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.R;

import java.util.List;

public class CandidateInfoAdapter extends ArrayAdapter<AddCandidate> {
    private Activity context;
    private List<AddCandidate> addCandidateList;
    public CandidateInfoAdapter(Activity context,List<AddCandidate>addCandidateList) {
        super(context, R.layout.candidate_list, addCandidateList);
        this.context = context;
        this.addCandidateList = addCandidateList;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View ListView=inflater.inflate(R.layout.candidate_list,null,true);

        TextView candidateName=ListView.findViewById(R.id.tvCandidateName);
        //TextView postName=(TextView)ListView.findViewById(R.id.tvCandidateName);
        AddCandidate addCandidate=addCandidateList.get(position);
        candidateName.setText(addCandidate.getCandidateName());
        //postName.setText(addCandidate.getPostName());

        return ListView;

    }

}
