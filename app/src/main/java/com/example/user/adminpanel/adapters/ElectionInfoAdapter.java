package com.example.user.adminpanel.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;

import java.util.List;

public class ElectionInfoAdapter extends ArrayAdapter<AddElection> {
    private Activity context;
    private List<AddElection>addElectionList;
    public ElectionInfoAdapter(Activity context,List<AddElection>addElectionList){
        super(context, R.layout.list_election,addElectionList);
        this.context=context;
        this.addElectionList=addElectionList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View ListView=inflater.inflate(R.layout.list_election,null,true);

        TextView electionId=(TextView)ListView.findViewById(R.id.tvElection_id);
        
        TextView electionDate=(TextView)ListView.findViewById(R.id.tv_date);
        TextView electionStartTime=(TextView)ListView.findViewById(R.id.tvStart_time);
        TextView electionEndTime=(TextView)ListView.findViewById(R.id.tvEnd_time);
        TextView electionStatus=(TextView)ListView.findViewById(R.id.tv_status);
        AddElection addElection=addElectionList.get(position);
        electionId.setText(addElection.getElectionId());

        electionDate.setText(addElection.getElectionDate());
        electionStartTime.setText(addElection.getStartTime());
        electionEndTime.setText(addElection.getEndTime());
        electionStatus.setText(addElection.getElectionStatus());
        return ListView;

        }
}
