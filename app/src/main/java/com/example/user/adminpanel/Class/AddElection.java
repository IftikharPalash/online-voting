package com.example.user.adminpanel.Class;

public class AddElection {
    String electionId,electionDate,startTime,endTime,electionStatus;
    public AddElection(){

    }

    public AddElection(String electionId,  String electionDate, String startTime, String endTime, String electionStatus) {
        this.electionId = electionId;

        this. electionDate = electionDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.electionStatus = electionStatus;
    }

    public String getElectionId() {
        return electionId;
    }


    public String getElectionDate() {
        return electionDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getElectionStatus() {
        return electionStatus;
    }
}
