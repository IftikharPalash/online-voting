package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.adapters.SaveDataList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PostRetrieved extends AppCompatActivity {
    private ListView listView;
    DatabaseReference databaseReference;
    DatabaseReference postRef;
    List<AddCandidate.PostModel>dataList;

    Query electionRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_retrieved);
        listView=findViewById(R.id.listViewSaveData);
        //databaseReference= FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Post");

        databaseReference = FirebaseDatabase.getInstance().getReference();
        electionRef = databaseReference.child("Election").limitToLast(1);

        dataList=new ArrayList<>();

}



//    PostModel presidentModel, vpModel;


    @Override
    protected void onStart() {
        super.onStart();

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                postRef = databaseReference.child("Election").child(election).child("Post");
                Log.v("Election: ", postRef.toString());
                postRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot:dataSnapshot.getChildren()){
                            AddCandidate.PostModel postModel =saveDataSnapshot.getValue(AddCandidate.PostModel.class);

                            dataList.add(postModel);

                        }

                        SaveDataList saveDataList=new SaveDataList(PostRetrieved.this,dataList);
                        listView.setAdapter(saveDataList);}
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
         listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
             @Override
             public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                 AddCandidate.PostModel postModel=dataList.get(position);
                 showUpdatePost( postModel.getPostName(),postModel. getElectionId(),postModel.getPostId(),postModel.getNumberOfPost());
                 return false;
             }
         });


    }

    private void showUpdatePost(final String PostName, String ElectionId,  String PostId, String NumberOfPost) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_post, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextElectionId= (EditText) dialogView.findViewById(R.id.editTextElectionId);
        final EditText editTextPostId = (EditText) dialogView.findViewById(R.id.editTextPostId);
        final EditText editTextPostNumber = (EditText) dialogView.findViewById(R.id.editTextPostNumber);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.updatePost);
        final Button btnDeletePost = (Button) dialogView.findViewById(R.id.deletePost);

        dialogBuilder.setTitle("Updating Post"+ElectionId+PostId+NumberOfPost);
        final AlertDialog alertDialog=dialogBuilder.create();
        alertDialog.show();

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String PostId=editTextPostId.getText().toString().trim();
                String PostNumber=editTextPostNumber.getText().toString().trim();
                String ElectionId=editTextElectionId.getText().toString().trim();

               if(TextUtils.isEmpty(PostId)){
                    editTextPostId.setError("PostId required");
                    return;
                }
              else if(TextUtils.isEmpty(ElectionId)){
                  editTextElectionId.setError("ElectionId required");
                  return;
              }
              else if(TextUtils.isEmpty(PostNumber)){
                  editTextPostNumber.setError("PostNumber required");
                  return;
              }
              updatePost(PostName,ElectionId,PostId,PostNumber);
              alertDialog.dismiss();
            }
        });
        btnDeletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePost(PostName);



            }
        });




    }

    private void deletePost(String postName) {
        DatabaseReference dtPost=FirebaseDatabase.getInstance().getReference("Post").child(postName);
        dtPost.removeValue();
        Toast.makeText(this,"Post Is Deleted",Toast.LENGTH_LONG).show();
    }

    private boolean updatePost( String postName ,String electionId,String postId,String numberOfPost){
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Post").child(postName);
        AddCandidate.PostModel postModel=new AddCandidate.PostModel(postName ,electionId,postId,numberOfPost);
        databaseReference.setValue(postModel);
        Toast.makeText(this,"Post Updated Successfully",Toast.LENGTH_LONG).show();
        return true;
    }
}
