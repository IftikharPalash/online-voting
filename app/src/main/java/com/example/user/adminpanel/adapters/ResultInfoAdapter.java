package com.example.user.adminpanel.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.adminpanel.Class.AddResult;
import com.example.user.adminpanel.R;

import java.util.List;

public class ResultInfoAdapter extends ArrayAdapter<AddResult> {
    private Activity context;
    private List<AddResult> addResultList;
    public ResultInfoAdapter(Activity context,List<AddResult>addResultList) {
        super(context, R.layout.result_list, addResultList);
        this.context = context;
        this.addResultList = addResultList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View ListView=inflater.inflate(R.layout.result_list,null,true);

        TextView candidateName=(TextView)ListView.findViewById(R.id.tvCandidateName);
        TextView electionId=(TextView)ListView.findViewById(R.id.tvElectionId);
        TextView postName=(TextView)ListView.findViewById(R.id.tvPostName);
        TextView totalVote=(TextView)ListView.findViewById(R.id.tvTotalVote);
        TextView voteCast=(TextView)ListView.findViewById(R.id.tvVoteCast);
        AddResult addResult=addResultList.get(position);
        candidateName.setText(addResult.getCandidateName());
        electionId.setText(addResult.getElectionId());
        postName.setText(addResult.getPostName());
        totalVote.setText(addResult.getTotalVote());
        voteCast.setText(addResult.getVoteCast());
        return ListView;

    }
}
