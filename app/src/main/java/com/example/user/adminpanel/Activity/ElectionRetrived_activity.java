package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.adapters.ElectionInfoAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ElectionRetrived_activity extends AppCompatActivity {
    private ListView listView;
    DatabaseReference databaseReference;
    List<AddElection>addElectionList;
    EditText etElectionYear,etElectionDate,etElcStartTime,etElcEndTime,etElcStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_election_retrived_activity);
        listView=findViewById(R.id.electionList_view);
        databaseReference= FirebaseDatabase.getInstance().getReference("Election");
        addElectionList=new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseReference.limitToLast(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot addElectionSnapshot:dataSnapshot.getChildren()){
                AddElection addElection=addElectionSnapshot.getValue(AddElection.class);
                addElectionList.add(addElection);

            }
            ElectionInfoAdapter electionInfoAdapter=new ElectionInfoAdapter(ElectionRetrived_activity.this,addElectionList);
            listView.setAdapter(electionInfoAdapter);}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                AddElection addElection=addElectionList.get(position);
                showUpdateElection( addElection.getElectionId(),addElection.getElectionDate()
                        ,addElection.getStartTime(),addElection.getEndTime(),addElection.getElectionStatus());
                return false;
            }
        });

    }
    private void showUpdateElection(final String ElectionId,  String ElectionDate,
                                    String EndTime, String StartTime, String ElectionStatus) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_election, null);
        dialogBuilder.setView(dialogView);


        final EditText etElectionDate = (EditText) dialogView.findViewById(R.id.etElectionDate);
        final EditText etElcStartTime= (EditText) dialogView.findViewById(R.id.etElcStartTime);
        final EditText etElcEndTime = (EditText) dialogView.findViewById(R.id.etElcEndTime);
        final EditText etElcStatus = (EditText) dialogView.findViewById(R.id.etElcStatus);
        final Button updateElection = (Button) dialogView.findViewById(R.id.updateElection);
        final Button btnDeleteElection = (Button) dialogView.findViewById(R.id.btnDeleteElection);

        dialogBuilder.setTitle("Updating Election"+ElectionDate+EndTime+StartTime+ElectionStatus);
        final AlertDialog alertDialog=dialogBuilder.create();
        alertDialog.show();



        updateElection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ElectionDate=etElectionDate.getText().toString().trim();
                String EndTime=etElcEndTime.getText().toString().trim();
                String StartTime=etElcStartTime.getText().toString().trim();
                String ElectionStatus=etElcStatus.getText().toString().trim();

                 if(TextUtils.isEmpty(ElectionDate)){
                    etElectionDate.setError("ElectionDate required");
                    return;
                }
                else if(TextUtils.isEmpty(EndTime)){
                    etElcEndTime.setError("EndTime required");
                    return;
                }
                else if(TextUtils.isEmpty(StartTime)){
                    etElcStartTime.setError("StartTime required");
                    return;
                }
                else if(TextUtils.isEmpty(ElectionStatus)){
                    etElcStatus.setError("Status required");
                    return;
                }

                updateElection(ElectionId,ElectionDate,StartTime,EndTime,ElectionStatus);
                alertDialog.dismiss();
                clearField();
            }
        });
        btnDeleteElection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteElection(ElectionId);

                
            }
        });


    }

    private void clearField() {

        etElectionDate.setText("");
        etElcEndTime.setText("");
        etElcStartTime.setText("");
        etElcStatus.setText("");
    }



    private void deleteElection(String electionId) {
        DatabaseReference drElection= FirebaseDatabase.getInstance().getReference("Election").child(electionId);
        drElection.removeValue();
        Toast.makeText(this,"Election Is Deleted",Toast.LENGTH_LONG).show();
        clearField();
    }

    private boolean updateElection( String electionId,String electionDate,String startTime, String endTime,String electionStatus){
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Election").child(electionId);
        AddElection addElection=new AddElection(electionId,electionDate,startTime,endTime,electionStatus);
        databaseReference.setValue(addElection);
        Toast.makeText(this,"Election Updated Successfully",Toast.LENGTH_LONG).show();
        return true;
    }


}
