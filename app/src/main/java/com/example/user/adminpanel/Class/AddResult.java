package com.example.user.adminpanel.Class;

public class AddResult {
    String candidateName,electionId,postName,totalVote,voteCast;
    public AddResult(){

    }


    public AddResult(String candidateName,String electionId,String postName,  String totalVote, String voteCast) {
        this.candidateName = candidateName;
        this.electionId = electionId;
        this.postName = postName;

        this.totalVote = totalVote;
        this.voteCast = voteCast;
    }

    public String getCandidateName() {
        return candidateName;
    }
    public String getElectionId() {
        return electionId;
    }


    public String getPostName() {
        return postName;
    }


    public String getTotalVote() {
        return totalVote;
    }

    public String getVoteCast() {
        return voteCast;
    }
}
