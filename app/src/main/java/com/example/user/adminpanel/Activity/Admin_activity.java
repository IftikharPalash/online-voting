package com.example.user.adminpanel.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v7.widget.CardView;


import com.example.user.adminpanel.R;
import com.google.firebase.auth.FirebaseAuth;

public class Admin_activity extends AppCompatActivity implements android.view.View.OnClickListener {

      private CardView voterView,postView,resultView,candidateView,electionView,electionList;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_activity);

        postView = findViewById(R.id.post_card);
        voterView = findViewById(R.id.voter_card);
        candidateView = findViewById(R.id.candidate_card);
        resultView = findViewById(R.id.result_card);
        electionView = findViewById(R.id.election_card);
        electionList = findViewById(R.id.electionList_card);
        mAuth = FirebaseAuth.getInstance();

        postView.setOnClickListener(this);
        voterView.setOnClickListener(this);
        candidateView.setOnClickListener(this);
        resultView.setOnClickListener(this);
        electionView.setOnClickListener(this);
        electionList.setOnClickListener(this);





        }

     @Override
    public void onClick(android.view.View v) {
        if(v.getId()==R.id.post_card)
        {
          Intent intent=new Intent(Admin_activity.this,PostActivity.class);
          startActivity(intent);

        }
        else if(v.getId()==R.id.voter_card)
        {
         Intent intent=new Intent(Admin_activity.this,VoterActivity.class);
          startActivity(intent);
        }
         else if(v.getId()==R.id.candidate_card)
        {
         Intent intent=new Intent(Admin_activity.this,CandidateActivity.class);
          startActivity(intent);
        }
         else if(v.getId()==R.id.result_card)
        {
         Intent intent=new Intent(Admin_activity.this,ResultRetrieved.class);
          startActivity(intent);
        }
         else if(v.getId()==R.id.election_card)
        {
         Intent intent=new Intent(Admin_activity.this,ElectionActivity.class);
          startActivity(intent);
        }
        else if(v.getId()==R.id.electionList_card)
        {
            Intent intent=new Intent(Admin_activity.this,ElectionList.class);
            startActivity(intent);
        }
  }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.menu,menu);
       return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.action_setting:
                FirebaseAuth.getInstance().signOut();
                finish();
                Toast.makeText(getApplicationContext(), "Logout successfully",Toast.LENGTH_SHORT).show();
                Intent action_setting=new Intent(this,LogIn_activity.class);
                startActivity(action_setting);

                break;
                default:
        }
        return super.onOptionsItemSelected(item);
    };







}
