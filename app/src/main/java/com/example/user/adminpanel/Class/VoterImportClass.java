package com.example.user.adminpanel.Class;

import com.example.user.adminpanel.Activity.LogIn_activity;
import com.google.firebase.database.core.Context;

public class VoterImportClass {
    private String name,id,email, password;
    String isVoted;

    public VoterImportClass() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String isVoted() {
        return isVoted;
    }

    public void setVoted(String voted) {
        isVoted = voted;
    }

    @Override
    public String toString() {
        return "VoterImportClass{" + "name='" + name + '\'' + ", id='" + id + '\'' + ", email='" + email + '\'' + ",isVoted='" + isVoted + '\'' + ", password='" + password + '\'' + '}';
    }
}
