package com.example.user.adminpanel.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.user.adminpanel.R;
import com.google.firebase.auth.FirebaseAuth;

public class UserActivity extends AppCompatActivity {
    private CardView ballotBox,result,history,about;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        ballotBox =findViewById(R.id.voteCardView);
        ballotBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BallotBox();
            }
        });
        result =findViewById(R.id.resultCardView);
        result.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View view) {
                ResultView();
            }
        });
//        history =findViewById(R.id.historyCardView);
//        history.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HistoryView();
//
//            }
//        });

//        about =findViewById(R.id.aboutCardView);
//        about.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AboutView();
//
//            }
//        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId())
        {
            case R.id.action_setting:
                Toast.makeText(getApplicationContext(), "Logout successfully",Toast.LENGTH_SHORT).show();
                Intent action_setting=new Intent(this,LogIn_activity.class);
                startActivity(action_setting);
                break;
            default:
        }
        return super.onOptionsItemSelected(item);
    };

    public void ResultView(){
        Intent intent=new Intent(this,ResultUserActivity.class);
        startActivity(intent);
    }
    public void BallotBox(){
        Intent intent=new Intent(this,BallotBox.class);
        startActivity(intent);
    }
//    public void HistoryView(){
//        Intent intent=new Intent(this,HistoryView.class);
//        startActivity(intent);
//    }
//    public void AboutView(){
//        Intent intent=new Intent(this,AboutView.class);
//        startActivity(intent);
//    }

}

