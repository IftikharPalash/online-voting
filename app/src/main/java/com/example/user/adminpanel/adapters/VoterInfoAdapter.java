package com.example.user.adminpanel.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.adminpanel.Class.VoterImportClass;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.Class.VoteAdd;

import java.util.List;

public class VoterInfoAdapter extends ArrayAdapter<VoterImportClass> {

    private Activity context;
    private List<VoterImportClass> voterImportClassList;
    public VoterInfoAdapter(Activity context,List<VoterImportClass>voterImportClassList){
        super(context, R.layout.voter_list,voterImportClassList);
        this.context=context;
        this.voterImportClassList=voterImportClassList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View ListView=inflater.inflate(R.layout.voter_list,null,true);

        TextView voterName=(TextView)ListView.findViewById(R.id.tvVoterName);
        TextView studentId=(TextView)ListView.findViewById(R.id.tvStudentId);
        //TextView voterSerial=(TextView)ListView.findViewById(R.id.tvVoterSerial);
        TextView voteStatus=(TextView)ListView.findViewById(R.id.tvVoteStatus);
        TextView voterEmail=(TextView)ListView.findViewById(R.id.tvVoterEmail);
        TextView voterPassword=(TextView)ListView.findViewById(R.id.tvVoterPassword);
        VoterImportClass voterImportClass=voterImportClassList.get(position);
        voterName.setText(voterImportClass.getName());
        studentId.setText(voterImportClass.getId());
        //voterSerial.setText(voteAdd.getVoterSerial());
        voteStatus.setText(String.valueOf(voterImportClass.isVoted()));
        voterEmail.setText(voterImportClass.getEmail());
        voterPassword.setText(voterImportClass.getPassword());
        return ListView;
    }
}
