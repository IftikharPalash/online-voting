package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.adapters.ElectionInfoAdapter;
import com.example.user.adminpanel.adapters.ElectionManageAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ElectionList extends AppCompatActivity  {


    List<AddElection> addElectionList;
    DatabaseReference databaseReference;
    ElectionManageAdapter electionManageAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_election_list);

        databaseReference= FirebaseDatabase.getInstance().getReference("Election");
        addElectionList=new ArrayList<>();

        /*
         * Using findViewById, we get a reference to our RecyclerView from xml. This allows us to
         * do things like set the adapter of the RecyclerView and toggle the visibility.
         */
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view_election);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        /*
         * Use this setting to improve performance if you know that changes in content do not
         * change the child layout size in the RecyclerView
         */
        recyclerView.setHasFixedSize(true);
        /*
         * The CourseAdapter is responsible for linking our course data with the Views that
         * will end up displaying our course data.
         */
        electionManageAdapter = new ElectionManageAdapter();

        recyclerView.setAdapter(electionManageAdapter);

        databaseReference.limitToLast(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                addElectionList.clear();
                for (DataSnapshot addElectionSnapshot:dataSnapshot.getChildren()){
                    AddElection addElection=addElectionSnapshot.getValue(AddElection.class);
                    addElection.getElectionId();
                        addElectionList.add(addElection);


                }
                electionManageAdapter.setElectionManageData(addElectionList);

                }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }




}
