package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.VoterImportClass;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.Class.VoteAdd;
import com.example.user.adminpanel.adapters.SaveDataList;
import com.example.user.adminpanel.adapters.VoterInfoAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class VoteRetrieved extends AppCompatActivity {
    private ListView listView;
    DatabaseReference voterRef;
    DatabaseReference databaseReference;
    List<VoterImportClass> voterImportClassList;
    Query electionRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote_retrieved);
        listView=findViewById(R.id.voterRetrievedList);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        electionRef = databaseReference.child("Election").limitToLast(1);
        //voterRef= FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Voter");
        voterImportClassList=new ArrayList<>();
    }

    @Override
    protected void onStart() {
        super.onStart();
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                voterRef = databaseReference.child("Election").child(election).child("Voter");
                Log.v("Election: ", voterRef.toString());
                voterRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot voterImportClassSnapshot:dataSnapshot.getChildren()){
                            VoterImportClass voterImportClass=voterImportClassSnapshot.getValue(VoterImportClass.class);
                            voterImportClassList.add(voterImportClass);

                        }
                        VoterInfoAdapter voterInfoAdapter=new VoterInfoAdapter(VoteRetrieved.this,voterImportClassList);
                        listView.setAdapter(voterInfoAdapter);}
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });


    }
}
