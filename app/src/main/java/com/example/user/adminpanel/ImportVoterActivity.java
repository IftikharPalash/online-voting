package com.example.user.adminpanel;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.user.adminpanel.Class.VoterImportClass;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ImportVoterActivity extends AppCompatActivity {
    private static final String TAG = "ImportVoterActivity";

    private static final int REQUEST_CODE = 6384;
    DatabaseReference upRef;

    Query lastElec;
    private  String[] lastElection = {""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_voter);
        readVoterData();
        Button button = new Button(this);
        button.setText(R.string.choose_file);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChooser();
            }
        });

        setContentView(button);
        upRef = FirebaseDatabase.getInstance().getReference();
        lastElec = upRef.child("Election").limitToLast(1);


        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        Log.v("ElectionKey: ", snapshot.getKey().toString());
                        lastElection[0] = snapshot.getKey();

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }; lastElec.addListenerForSingleValueEvent(valueEventListener);
    }
    private void showChooser() {
        Intent target = FileUtils.createGetContentIntent();
        Intent intent = Intent.createChooser(
                target, getString(R.string.chooser_title));
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        final Uri uri = data.getData();
                        Log.i(TAG, "Uri = " + uri.toString());
                        try {
                            final String path = FileUtils.getPath(this, uri);

                            readCsv(path);

                            Toast.makeText(ImportVoterActivity.this,
                                    "File Selected: " + path, Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Log.e("ImportVoterActivity", "File select error", e);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void readCsv(String path) {
        ArrayList<VoterImportClass> data = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File(path));
            int i = 0;
            while (sc.hasNextLine()) {
                String[] cells = sc.nextLine().split(",");
                VoterImportClass voter = new VoterImportClass();
                voter.setName(cells[0]);
                voter.setId(cells[1]);
                voter.setEmail(cells[2]);
                voter.setPassword(((int)Math.floor((Math.random()+1)*1000000)) + "");
                voter.setVoted("false");
                Log.e(TAG, "readCsv:" + voter.toString() );
                data.add(voter);
            }
            storeVoterData(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void storeVoterData(ArrayList<VoterImportClass> data) {
        DatabaseReference voterRef = FirebaseDatabase.getInstance().getReference().child("Election").child(lastElection[0]).child("Voter");
        for (VoterImportClass voter : data) {
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(voter.getEmail(), voter.getPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        Log.e(TAG, "onComplete: "+ task.getException().getMessage() );
//                        Toast.makeText(ImportVoterActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
            voterRef.child(voter.getId()).setValue(voter);

        }

    }

    private List<VoterImportClass> voterImportClassList = new ArrayList<>();
    private void readVoterData() {

            InputStream is = getResources().openRawResource(R.raw.voter);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is, Charset.forName("UTF-8"))
            );

            String line = null;
            try {

                while ((line = reader.readLine()) != null) {

                    String[] tokens = line.split(",");


                    VoterImportClass info = new VoterImportClass();
                    info.setName(tokens[0]);
                    info.setId(tokens[1]);
                    info.setEmail(tokens[2]);
                    info.setPassword((int)(Math.random()*1000000.0) + "");

                    voterImportClassList.add(info);

                    Log.d("ImportVoterActivity","just created: " +info.toString());

                }
            }catch (IOException e){

                Log.wtf("ImportVoterActivity","Error reading data file on line" + line, e);
                e.printStackTrace();

            }
        }
    }




