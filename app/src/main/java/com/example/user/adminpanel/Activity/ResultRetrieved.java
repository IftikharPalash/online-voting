package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddResult;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.adapters.ResultInfoAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultRetrieved extends AppCompatActivity {
    private ListView listView;
    DatabaseReference resultRef;
   DatabaseReference resultReferenceData;
    Query electionRef;
    Button btnResultGenerate;
    private  ArrayList<String>mAdminResult =new ArrayList<>();

    private ArrayAdapter<String> arrayAdapter;

    List<AddResult> addResultList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_retrieved);
        listView = findViewById(R.id.listResultRetrieved);
        btnResultGenerate = findViewById(R.id.btn_result_generate);
        //databaseReference= FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Result");
        addResultList = new ArrayList<>();
        resultReferenceData = FirebaseDatabase.getInstance().getReference();
        electionRef = resultReferenceData.child("Election").limitToLast(1);
        //databaseReference= FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Result");
        arrayAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,mAdminResult);
        listView.setAdapter(arrayAdapter);

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                resultRef = resultReferenceData.child("Election").child(election).child("Result");
                Log.v("Election: ", resultRef.toString());
                resultRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot data : dataSnapshot.getChildren()){


                            mAdminResult.add(data.getKey()+(data.getValue()));
                            Log.v("getkey: ",data.getKey());
                            Log.v("getkey: ",data.getValue().toString());

                           // mAdminResult.add(data.getValue().toString());
                        }
                        arrayAdapter.notifyDataSetChanged();


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });


        btnResultGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultGenerate();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

//        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                AddResult addResult = addResultList.get(position);
//                showUpdateResult(addResult.getCandidateName(), addResult.getElectionId(), addResult.getPostName(), addResult.getTotalVote()
//                        , addResult.getVoteCast());
//                return false;
//            }
//        });

    }

//    private void showUpdateResult(final String CandidateName, String ElectionId, String PostName, String TotalVote,
//                                  String VoteCast) {
//
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
//        LayoutInflater inflater = getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.update_result, null);
//        dialogBuilder.setView(dialogView);
//
//        final EditText etElectionId = (EditText) dialogView.findViewById(R.id.eTextElectionId);
//        final EditText etPostName = (EditText) dialogView.findViewById(R.id.editTextPostName);
//        final EditText etTotalVote = (EditText) dialogView.findViewById(R.id.editTextTotalVote);
//        final EditText etVoteCast = (EditText) dialogView.findViewById(R.id.editTextVoteCast);
//        final Button updateResult = (Button) dialogView.findViewById(R.id.updateResult);
//        final Button btnDeleteResult = (Button) dialogView.findViewById(R.id.deleteResult);
//
//        dialogBuilder.setTitle("Updating Result " + ElectionId + PostName + TotalVote + VoteCast);
//        final AlertDialog alertDialog = dialogBuilder.create();
//        alertDialog.show();
//
//        updateResult.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                String ElectionId = etElectionId.getText().toString().trim();
//                String PostName = etPostName.getText().toString().trim();
//                String TotalVote = etTotalVote.getText().toString().trim();
//                String VoteCast = etVoteCast.getText().toString().trim();
//
//                if (TextUtils.isEmpty(ElectionId)) {
//                    etElectionId.setError("PostName required");
//                    return;
//                } else if (TextUtils.isEmpty(PostName)) {
//                    etPostName.setError("PostName required");
//                    return;
//                } else if (TextUtils.isEmpty(TotalVote)) {
//                    etTotalVote.setError("TotalVote required");
//                    return;
//                } else if (TextUtils.isEmpty(VoteCast)) {
//                    etVoteCast.setError("VoteCast required");
//                    return;
//                }
//
//                updateResult(CandidateName, ElectionId, PostName, TotalVote, VoteCast);
//                alertDialog.dismiss();
//            }
//        });
//        btnDeleteResult.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                deleteResult(CandidateName);
//            }
//        });
//
//    }

    private void resultGenerate() {
        DatabaseReference electionReference = FirebaseDatabase.getInstance().getReference("Election");
        electionReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                DatabaseReference currentElectionReference = null;
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    currentElectionReference = child.getRef();
                }
                countVotes(currentElectionReference);
            }

            private void countVotes(final DatabaseReference currentElectionReference) {
                if (currentElectionReference == null) return;

                final Map<CharSequence, Map<CharSequence, Integer>> votes = new HashMap<>();

                DatabaseReference votesReference = currentElectionReference.child("Votes");
                votesReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot vote : dataSnapshot.getChildren()) {
                            for (DataSnapshot post : vote.getChildren()) {
                                if(!votes.containsKey(post.getKey()))
                                    votes.put(post.getKey(), new HashMap<CharSequence, Integer>());
                                for (DataSnapshot candidate : post.getChildren()) {
                                    if (votes.get(post.getKey()).containsKey((String)candidate.getValue())) {
                                        votes.get(post.getKey()).put((String) candidate.getValue(), votes.get(post.getKey()).get(candidate.getValue()) + 1);
                                    } else {
                                        votes.get(post.getKey()).put((String) candidate.getValue(), 1);
                                    }
                                }
                            }
                        }
                        currentElectionReference.child("Result").setValue(votes);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

//    private void deleteResult(String candidateName) {
//        DatabaseReference drResult = FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Result").child(candidateName);
//        drResult.removeValue();
//        Toast.makeText(this, "Result Is Deleted", Toast.LENGTH_LONG).show();
//    }

//    private boolean updateResult(String candidateName, String electionId, String postName, String totalVote, String voteCast) {
//        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Election").child(electionId).child("Result").child(candidateName);
//        AddResult addResult = new AddResult(candidateName, electionId, postName, totalVote, voteCast);
//        databaseReference.setValue(addResult);
//        Toast.makeText(this, "Result Updated Successfully", Toast.LENGTH_LONG).show();
//        return true;
//    }
}
