package com.example.user.adminpanel.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class CandidateActivity extends AppCompatActivity {

    ArrayList<String> postList = new ArrayList<>();
    ArrayList<String> electionIdList = new ArrayList<>();
    ArrayList<String> candiPostIdList = new ArrayList<>();
    private EditText candidateName, candidateId;
    private static final int PICK_IMAGE_REQUEST = 234;
    private Button addCandidate, showCandidate;
    private Spinner spPosts;
    private Spinner candiPostId;
    private String selectedPost;
    private String selectedCandiPost;
    private Spinner eleCandiId;
    private String selectedCandiElectionId;
    DatabaseReference candidateReference;
    DatabaseReference postsReference;
    DatabaseReference electionReference;
    DatabaseReference dataPostRef;
    ArrayAdapter spinnerElecAdapter;
    ArrayAdapter spinnerCandiPostAdapter;
    Query electionRef;


    ArrayAdapter spinnerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate);

        init();

        showCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CandidateRetrieved();
            }
        });

       // selectedPost = electionId.getText().toString();


        addCandidate = (Button) findViewById(R.id.btnCandidateAdd);
        addCandidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CandidateActivity.this, "Added Successfully", Toast.LENGTH_SHORT).show();
                AddCandidate();
                clearFields();


            }
        });
        populateCandiElectionIdSpinner();
        electionReference.limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AddElection addElection = dataSnapshot.getValue(AddElection.class);
                electionIdList.add(addElection.getElectionId());
                spinnerElecAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        }

    private void clearFields() {
        candidateName.setText("");
       // postId.setText("");
      //  electionId.setText("");
        candidateId.setText("");


    }

    private void init() {
        candidateName = (EditText) findViewById(R.id.etCandidateName);
       // postId = (EditText) findViewById(R.id.etPostId);
       // electionId = (EditText) findViewById(R.id.etElectionId);
        candidateId = (EditText) findViewById(R.id.etCandidateId);
        eleCandiId = findViewById(R.id.elecandi_id);
        candiPostId = findViewById(R.id.postcandi_id);

        showCandidate = (Button) findViewById(R.id.btnShowCandidate);
        spPosts = findViewById(R.id.sp_posts);
        dataPostRef = FirebaseDatabase.getInstance().getReference();
        electionRef = dataPostRef.child("Election").limitToLast(1);

        candidateReference = FirebaseDatabase.getInstance().getReference();
        electionReference=FirebaseDatabase.getInstance().getReference().child("Election");
       // postsReference = FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Post");


        populatePostSpinner();
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                postsReference = dataPostRef.child("Election").child(election).child("Post");
                Log.v("Election: ", postsReference.toString());
                postsReference.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        AddCandidate.PostModel postModel = dataSnapshot.getValue(AddCandidate.PostModel.class);
                        postList.add(postModel.getPostName());
                        //candidateReference.getKey();
                        spinnerAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

        populateCandiPostSpinner();

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                postsReference = dataPostRef.child("Election").child(election).child("Post");
                Log.v("Election: ", postsReference.toString());
                postsReference.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        AddCandidate.PostModel postModel = dataSnapshot.getValue(AddCandidate.PostModel.class);
                        candiPostIdList.add(postModel.getPostId());
                        //candidateReference.getKey();
                        spinnerCandiPostAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });


    }


    private void populatePostSpinner() {
        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, postList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spPosts.setAdapter(spinnerAdapter);
        spPosts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPost = postList.get(i);
                candiPostId.setSelection(i);
                spinnerCandiPostAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    protected void AddCandidate() {

        String ElectionId = selectedCandiElectionId;
        String PostId = selectedCandiPost;
        String CandidateName = candidateName.getText().toString().trim();
        String CandidateId = candidateId.getText().toString().trim();


        if(  CandidateId.isEmpty() ||  CandidateName.isEmpty()){
            Toast.makeText(CandidateActivity.this, "Please fill out all fields", Toast.LENGTH_SHORT).show();
            return;

        }

        AddCandidate addCandidate = new AddCandidate(ElectionId, PostId,CandidateName,  CandidateId);
        candidateReference.child("Election").child(ElectionId).child("Candidates").child(selectedPost).child(CandidateName).setValue(addCandidate);
    }

    public void CandidateRetrieved() {
        Intent intent = new Intent(this, CandidateRetrieved.class);
        startActivity(intent);
    }

    private void populateCandiPostSpinner() {
        spinnerCandiPostAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, candiPostIdList);
        spinnerCandiPostAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        candiPostId.setAdapter(spinnerCandiPostAdapter);
        candiPostId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCandiPost = candiPostIdList.get(i);
                spPosts.setSelection(i);
                spinnerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void populateCandiElectionIdSpinner() {
        spinnerElecAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, electionIdList);
        spinnerElecAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        eleCandiId.setAdapter(spinnerElecAdapter);
        eleCandiId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCandiElectionId = electionIdList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

}
