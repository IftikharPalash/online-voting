package com.example.user.adminpanel.Class;

import java.io.Serializable;

public class AddCandidate {
    String  electionId,postId,candidateName,  candidateId;

    public AddCandidate() {

    }

    public AddCandidate(String electionId,String postId, String candidateName,  String candidateId) {

        this.electionId = electionId;

        this.postId = postId;
        this.candidateName = candidateName;
        this.candidateId = candidateId;
    }


    public String getElectionId() {
        return electionId;
    }
    public String getPostId() {
        return postId;
    }

    public String getCandidateName() {
        return candidateName;
    }


    public String getCandidateId() {
        return candidateId;
    }

    public static class PostModel implements Serializable {


        String electionId,postName, postId, numberOfPost;

        public PostModel() {

        }

        public PostModel(String electionId,String postName, String postId,  String numberOfPost) {
            this.electionId = electionId;
            this.postName = postName;
            this.postId = postId;

            this.numberOfPost = numberOfPost;
        }

        public String getElectionId() {
            return electionId;
        }

        public String getPostName() {
            return postName;
        }

        public String getPostId() {
            return postId;
        }

        public String getNumberOfPost() {
            return numberOfPost;
        }

    }
}
