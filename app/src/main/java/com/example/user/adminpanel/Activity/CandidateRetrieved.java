package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CandidateRetrieved extends AppCompatActivity {
    private ListView listView;
    DatabaseReference candiRef;
    DatabaseReference candidateReference;
    Query electionRef;

    private  ArrayList<String>candiRet =new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;

//    List<String> addCandidateList;
//    ArrayAdapter<String>adapter;
//    AddCandidate addCandidate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_retrieved);
        listView=findViewById(R.id.candidateList_view);
        arrayAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,candiRet);
        listView.setAdapter(arrayAdapter);
        //candiRef= FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Candidates");
        candidateReference = FirebaseDatabase.getInstance().getReference();
        electionRef = candidateReference.child("Election").limitToLast(1);
    }
    @Override
    protected void onStart() {
        super.onStart();
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                candiRef = candidateReference.child("Election").child(election).child("Candidates");
                Log.v("Election: ", candiRef.toString());
                candiRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot data : dataSnapshot.getChildren()){
                            candiRet.add(data.getKey());

                            candiRet.add(String.valueOf(data.getValue()));

                        }
                        arrayAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

    }
}
