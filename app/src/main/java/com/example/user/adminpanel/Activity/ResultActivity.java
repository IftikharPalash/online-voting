package com.example.user.adminpanel.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddResult;
import com.example.user.adminpanel.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ResultActivity extends AppCompatActivity {
//    private EditText postName,candidateName,totalVote,voteCast,electionId;
//    private Button addResult;
    private Button showResult;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
//        postName=(EditText)findViewById(R.id.postNameText);
//        electionId=(EditText)findViewById(R.id.edElectionId);
//        candidateName=(EditText)findViewById(R.id.candidateNameText);
//        totalVote=(EditText)findViewById(R.id.totalVoteText);
//        voteCast=(EditText)findViewById(R.id.voteCastText);
//        addResult=(Button) findViewById(R.id.addResultBtn);
        showResult=(Button) findViewById(R.id.btnShowResult);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        showResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next=new Intent(ResultActivity.this,ResultRetrieved.class);
                startActivity(next);
            }
        });
//        addResult.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AddData();
//                Toast.makeText(ResultActivity.this,"Added Successfully",Toast.LENGTH_LONG).show();
//                clearFields();
//
//            }
//        });



    }
//    private void clearFields() {
//        candidateName.setText("");
//        electionId.setText("");
//        postName.setText("");
//        totalVote.setText("");
//        voteCast.setText("");
//
//    }

//    protected void AddData(){
//        String CandidateName=candidateName.getText().toString().trim();
//        String ElectionId=electionId.getText().toString().trim();
//        String PostName=postName.getText().toString().trim();
//
//        String TotalVote=totalVote.getText().toString().trim();
//        String VoteCast=voteCast.getText().toString().trim();
//
//        AddResult addResult=new AddResult(CandidateName,ElectionId,PostName,TotalVote,VoteCast);
//
//        databaseReference.child("Election").child(ElectionId).child("Result").child(CandidateName).setValue(addResult);
//
//
//    }
}
