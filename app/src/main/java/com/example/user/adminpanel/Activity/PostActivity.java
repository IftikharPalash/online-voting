package com.example.user.adminpanel.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class PostActivity extends AppCompatActivity {
    private EditText postId,numberOfPost;
    private Button addPost;
    private Button showPost;
    private Spinner elcId;
    private Spinner postName;

    private String selectedElectionId;
    DatabaseReference databaseReference;
    DatabaseReference electionReference;
    ArrayAdapter spinnerAdapter;

    ArrayList<String> electionIdList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        databaseReference=FirebaseDatabase.getInstance().getReference();
        electionReference=FirebaseDatabase.getInstance().getReference().child("Election");
        elcId = findViewById(R.id.elect_id);
        postName= findViewById(R.id.postSelect_id);
        postId=(EditText)findViewById(R.id.etPostId);
        numberOfPost=(EditText) findViewById(R.id.etNumberOfPost);
        // electionId=findViewById(R.id.etElectionId);
        addPost=(Button)findViewById(R.id.btnAddPost) ;
        showPost=(Button)findViewById(R.id.btnShowPost) ;
        showPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next=new Intent(PostActivity.this,PostRetrieved.class);
                startActivity(next);
            }
        });

        addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddData();
                Toast.makeText(PostActivity.this,"Added Successfully",Toast.LENGTH_SHORT).show();
                clearFields();

            }
        });
        populateElectionIdSpinner();
        electionReference.limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                AddElection addElection = dataSnapshot.getValue(AddElection.class);
                electionIdList.add(addElection.getElectionId());
                spinnerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
    private void clearFields() {
        numberOfPost.setText("");
        //postName.setText("");
        postId.setText("");
        //  electionId.setText("");


    }





    protected void AddData(){
        String ElectionId=selectedElectionId;
        String PostName=postName.getSelectedItem().toString().trim();
        String PostId=postId.getText().toString().trim();
        String NumberOfPost=numberOfPost.getText().toString().trim();
//        String ElectionId=electionId.getText().toString().trim();



        AddCandidate.PostModel postModel =new AddCandidate.PostModel(ElectionId,PostName,PostId,NumberOfPost);
        databaseReference.child("Election").child(ElectionId).child("Post").child(PostName).setValue(postModel);

    }
    private void populateElectionIdSpinner() {
        spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, electionIdList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        elcId.setAdapter(spinnerAdapter);
        elcId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedElectionId = electionIdList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



}
