package com.example.user.adminpanel.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.R;

import java.util.List;

public class SaveDataList extends ArrayAdapter<AddCandidate.PostModel> {
    private Activity context;
    private List<AddCandidate.PostModel> postModelList;
    public SaveDataList(Activity context,List<AddCandidate.PostModel> postModelList){
        super(context, R.layout.list_layout, postModelList);
        this.context=context;
        this.postModelList = postModelList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater= context.getLayoutInflater();

        View listViewItem=inflater.inflate(R.layout.list_layout,null,true);

        TextView postNameView=(TextView)listViewItem.findViewById(R.id.postNameView);
        TextView postIdView=(TextView)listViewItem.findViewById(R.id.postIdView);
        TextView electionIdView=(TextView)listViewItem.findViewById(R.id.electionIdView);
        TextView numberOfPostView=(TextView)listViewItem.findViewById(R.id.numberOfPostView);

        AddCandidate.PostModel postModel = postModelList.get(position);
        postNameView.setText(postModel.getPostName());
        postIdView.setText(postModel.getPostId());
        electionIdView.setText(postModel.getElectionId());
        numberOfPostView.setText(postModel.getNumberOfPost());
        return listViewItem;



    }
}
