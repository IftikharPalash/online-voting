package com.example.user.adminpanel.Activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.AddResult;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.adapters.ResultInfoAdapter;
import com.example.user.adminpanel.adapters.SaveDataList;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ResultUserActivity extends AppCompatActivity {
    private ListView listView;
    private DatabaseReference voterShowRef;
    DatabaseReference electionResultRef;
    Query electionRef;

    private  ArrayList<String>mUserResult =new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_user);

        listView=findViewById(R.id.listResultUserRetrieved);
        electionResultRef = FirebaseDatabase.getInstance().getReference();
        electionRef = electionResultRef.child("Election").limitToLast(1);
       // voterShowRef= FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Result");
        arrayAdapter= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,mUserResult);
        listView.setAdapter(arrayAdapter);
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                voterShowRef = electionResultRef.child("Election").child(election).child("Result");
                Log.v("Election: ", voterShowRef.toString());
                voterShowRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot data : dataSnapshot.getChildren()){

                            mUserResult.add(data.getKey()+(data.getValue()));
                        }
                        arrayAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });




    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}

