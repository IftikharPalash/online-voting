package com.example.user.adminpanel.Activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class ElectionActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText electionId;
    private  TextView electionDate;
    private  Spinner electionStatus;
    private  TextView startTime;
    private  TextView endTime;
    private Button addElection;
    DatabaseReference databaseReference;
    private Button showData;
    private  int mYear,mMonth,mDay,mHour,mMinute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_election);
        electionId=(EditText)findViewById(R.id.electionIdText);
       // electionYear=(EditText)findViewById(R.id.electionYearText);
        electionDate=(TextView)findViewById(R.id.dateText);
        startTime=(TextView) findViewById(R.id.startTimeText);
        endTime=(TextView) findViewById(R.id.endTimeText);
        electionStatus=(Spinner)findViewById(R.id.sp_elec_status);
        addElection=(Button) findViewById(R.id.btnAddElection);
        databaseReference = FirebaseDatabase.getInstance().getReference("Election");
        showData=(Button)findViewById(R.id.btn_show);
        electionDate.setOnClickListener(this);
        startTime.setOnClickListener(this);
        endTime.setOnClickListener(this);
        showData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next=new Intent(ElectionActivity.this,ElectionRetrived_activity.class);
                startActivity(next);
            }
        });

        addElection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(ElectionActivity.this,"added Successfully",Toast.LENGTH_LONG).show();
                addElection();
                clearFields();


            }
        });

    }
    private void clearFields() {
        electionId.setText("");
        //electionYear.setText("");
        electionDate.setText("");
        startTime.setText("");
        endTime.setText("");
       // electionStatus.setText("");



    }

    protected void addElection(){
        String ElectionId=electionId.getText().toString().trim();


        String ElectionDate=electionDate.getText().toString().trim();

        String StartTime=startTime.getText().toString().trim();

        String EndTime=endTime.getText().toString().trim();

        String ElectionStatus=electionStatus.getSelectedItem().toString().trim();


        //String ElectionId=databaseReference.push().getKey();
        if(ElectionId.isEmpty()  || ElectionDate.isEmpty() || StartTime.isEmpty()|| EndTime.isEmpty()|| ElectionStatus.isEmpty()){
            Toast.makeText(ElectionActivity.this, "Please fill out all fields", Toast.LENGTH_SHORT).show();
            return;

        }

        AddElection addElection=new AddElection(ElectionId,ElectionDate,StartTime,EndTime,ElectionStatus);
        databaseReference.child(ElectionId).setValue(addElection);



    }

    @Override
    public void onClick(View v) {
        if(v==electionDate){
            final Calendar calendar=Calendar.getInstance();
            mYear=calendar.get(Calendar.YEAR);
            mMonth=calendar.get(Calendar.MONTH);
            mDay=calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    electionDate.setText(dayOfMonth+"-"+(month+1)+"-"+year);
                }


            },mYear,mMonth,mDay);
            datePickerDialog.show();
        }
        if(v==startTime){
            final Calendar c=Calendar.getInstance();
            mHour=c.get(Calendar.HOUR_OF_DAY);
            mMinute=c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog=new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    String AM_PM ;
                    if(hourOfDay > 12) {
                        AM_PM = "PM";
                        hourOfDay=hourOfDay-12;
                        startTime.setText(hourOfDay+":"+minute+" "+AM_PM);

                    }else if(hourOfDay ==12){

                        AM_PM = "PM";
                        startTime.setText(hourOfDay+":"+minute+" "+AM_PM);


                    }
                    else {
                        AM_PM = "AM";
                        mHour=mHour-12;
                        startTime.setText(hourOfDay+":"+minute+" "+AM_PM);

                    }
                }
            },mHour,mMinute, false);
            timePickerDialog.show();
        }
        if(v==endTime){
            final Calendar c=Calendar.getInstance();
            mHour=c.get(Calendar.HOUR_OF_DAY);
            mMinute=c.get(Calendar.MINUTE);


            TimePickerDialog timePickerDialog=new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    String AM_PM ;
                    if(hourOfDay > 12) {
                        AM_PM = "PM";
                        hourOfDay=hourOfDay-12;
                        endTime.setText(hourOfDay+":"+minute+" "+AM_PM);

                    }else if(hourOfDay ==12) {

                        AM_PM = "PM";
                        endTime.setText(hourOfDay + ":" + minute + " " + AM_PM);
                    } else {
                        AM_PM = "AM";
                        mHour=mHour-12;
                        endTime.setText(hourOfDay+":"+minute+" "+AM_PM);

                    }
                }
            },mHour,mMinute, false);

            timePickerDialog.show();
        }


    }
}
