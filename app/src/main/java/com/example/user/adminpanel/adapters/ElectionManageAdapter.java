package com.example.user.adminpanel.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.example.user.adminpanel.Class.AddElection;
import com.example.user.adminpanel.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import android.widget.TextView;

import java.util.List;

public class ElectionManageAdapter extends RecyclerView.Adapter<ElectionManageAdapter.ElectionManageAdapterViewHolder> {
    //firebase database reference

    private DatabaseReference electionDatabaseRef;


    private Context context;
    private List<AddElection> electionIdList;
    private AddElection enabledElection;

    /**
     * Default Constructor for CourseAdapter
     */
    public ElectionManageAdapter() {
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param i  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout. See
     *                  {@link android.support.v7.widget.RecyclerView.Adapter#getItemViewType(int)}
     *                  for more details.
     * @return A new CourseAdapterViewHolder that holds the View for each list item
     */

    @NonNull
    @Override
    public ElectionManageAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.election_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        electionDatabaseRef = FirebaseDatabase.getInstance().getReference("Election");

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new ElectionManageAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ElectionManageAdapterViewHolder electionManageAdapterViewHolder, int i) {
        //Initialization and setting the course data into views.
        final AddElection election = electionIdList.get(i);
        electionManageAdapterViewHolder.electionIdTextView.setText(election.getElectionId());
        if(election.getElectionStatus().equals("Enable")) {
            electionManageAdapterViewHolder.electionSwitch.setChecked(true);
        } else {
            electionManageAdapterViewHolder.electionSwitch.setChecked(false);
        }
        electionManageAdapterViewHolder.electionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    electionDatabaseRef.child(election.getElectionId()).child("electionStatus").setValue("Enable");
                   // electionDatabaseRef.child(enabledElection.getElectionId()).child("electionStatus").setValue("Disable");
                } else {
                    electionDatabaseRef.child(election.getElectionId()).child("electionStatus").setValue("Disable");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if ( null == electionIdList) return 0;
        return electionIdList.size();
    }

    public void setElectionManageData(List<AddElection> data) {
        for (AddElection addElection: data){
            if(addElection.getElectionStatus().equals("Enable")) {
                enabledElection = addElection;
                break;
            }

        }
        electionIdList = data;
        notifyDataSetChanged();
    }

    /**
     * Cache of the children views for a course list item.
     */
    public class ElectionManageAdapterViewHolder extends RecyclerView.ViewHolder {

        public final TextView electionIdTextView;
        public final Switch electionSwitch;


        public ElectionManageAdapterViewHolder(final View itemView) {
            super(itemView);
            electionIdTextView = (TextView) itemView.findViewById(R.id.election_id);
            electionSwitch = (Switch) itemView.findViewById(R.id.election_switch);

        }



    }








}
