package com.example.user.adminpanel.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.VoterImportClass;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.adapters.SaveDataList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BallotBox extends AppCompatActivity implements View.OnClickListener {
    // private ListView lVPresident;
    private TextView presView,vPresView,gsView,jsView,tsrView,pabView,mmbView;
    private TextView lVPresident,lvVp,lvGs,lvJs,lvTsRr,lvPaB,lvMmb;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference voteReference;
    DatabaseReference newReference;
    DatabaseReference numberOfPostRef;
    DatabaseReference logInRef;
    DatabaseReference forVoteRef;
    DatabaseReference upRef;
    private SharedPreferences sharedPreferences;
    Query logQuery;

    Query lastElec;
    private  String[] lastElection = {""};


    List<AddCandidate.PostModel> dataList;
    AddCandidate.PostModel presidentModel, vpModel, tsModel, gsModel, pcModel, jsModel,mmbModel;


    LinearLayout candPresident;
    LinearLayout candVicePresident;
    LinearLayout candGeneralSecretary;
    LinearLayout candTreasurer;
    LinearLayout candJoinSecretary;
    LinearLayout candPublication;
    LinearLayout candMember;


    int candCountPresident = 0;
    int candCountVicePresident = 0;
    int candCountGeneralSecretary = 0;
    int candCountTreasurer = 0;
    int candCountJoinSecretary = 0;
    int candCountPublication = 0;
    int candCountMember = 0;


    DatabaseReference databaseReference;
    // DatabaseReference postsReference;
    Query electionRef;
    DatabaseReference dataRef;
    private String uid;
    private boolean voteStatus=true;



    List<AddCandidate> addCandidateList;
    List<AddCandidate> addCandidateList1;
    List<AddCandidate> addCandidateList2;
    List<AddCandidate> addCandidateList3;
    List<AddCandidate> addCandidateList4;
    List<AddCandidate> addCandidateList5;
    List<AddCandidate> addCandidateList6;

    List<String> selectedPresident = new ArrayList<>();
    List<String> selectedVicePresident = new ArrayList<>();
    List<String> selectedGeneralSecretary = new ArrayList<>();
    List<String> selectedJoinSecretary = new ArrayList<>();
    List<String> selectedTreasurer = new ArrayList<>();
    List<String> selectedPublication = new ArrayList<>();
    List<String> selectedMember = new ArrayList<>();




    Button btnVote;

    List<AddCandidate.PostModel> postList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ballot_box);
        lVPresident=findViewById(R.id.lVPresiNmOfPost);
        lvGs=findViewById(R.id.lvGSNmOfPost);
        lvJs=findViewById(R.id.lvJsNmOfPost);
        lvPaB=findViewById(R.id.lvPaBNmOfPost);
        lvTsRr=findViewById(R.id.lvTreasurerNmOfPost);
        lvVp=findViewById(R.id.lvVcPresiNmOfPost);
        lvMmb=findViewById(R.id.mMbNmOfPost);
        mmbView=findViewById(R.id.mMb_view);
        presView=findViewById(R.id.pres_view);
        vPresView=findViewById(R.id.vPres_View);
        gsView=findViewById(R.id.gs_view);
        tsrView=findViewById(R.id.tsr_view);
        jsView=findViewById(R.id.js_view);
        pabView=findViewById(R.id.pab_view);
        upRef = FirebaseDatabase.getInstance().getReference();
        lastElec = upRef.child("Election").limitToLast(1);

        sharedPreferences = getSharedPreferences("storage", Context.MODE_PRIVATE);
        uid=sharedPreferences.getString("id",null);






        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                        Log.v("ElectionKey: ", snapshot.getKey().toString());
                        lastElection[0] = snapshot.getKey();

                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }; lastElec.addListenerForSingleValueEvent(valueEventListener);



        firebaseDatabase = FirebaseDatabase.getInstance();
        voteReference = firebaseDatabase.getReference("vote");


        candPresident = findViewById(R.id.cand_president);

        candVicePresident = findViewById(R.id.cand_VicePresident);
        candGeneralSecretary = findViewById(R.id.cand_GeneralSecretary);
        candJoinSecretary = findViewById(R.id.cand_JoinSecretary);
        candPublication = findViewById(R.id.cand_Publication);
        candTreasurer = findViewById(R.id.cand_Treasurer);
        candMember = findViewById(R.id.cand_Member);
        btnVote = findViewById(R.id.btnVote);
        dataList = new ArrayList<>();




        databaseReference = FirebaseDatabase.getInstance().getReference();
        forVoteRef=FirebaseDatabase.getInstance().getReference();
        logQuery=forVoteRef.child("Election").limitToLast(1);

        // postsReference = FirebaseDatabase.getInstance().getReference().child("Election").child("E-1").child("Post");
        newReference = FirebaseDatabase.getInstance().getReference();
        numberOfPostRef=FirebaseDatabase.getInstance().getReference();

        dataRef=FirebaseDatabase.getInstance().getReference();
        electionRef=dataRef.child("Election").limitToLast(1);


        addCandidateList = new ArrayList<>();
        addCandidateList1 = new ArrayList<>();
        addCandidateList2 = new ArrayList<>();
        addCandidateList3 = new ArrayList<>();
        addCandidateList4 = new ArrayList<>();
        addCandidateList5 = new ArrayList<>();
        addCandidateList6 = new ArrayList<>();

        postList = new ArrayList<>();

        btnVote.setOnClickListener(this);

        logQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                logInRef = forVoteRef.child("Election").child(election).child("Voter").child(uid).child("isVoted");
                Log.v("logVote: ", logInRef.toString());
                logInRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Log.v("testforVoter: ", dataSnapshot.getValue().toString());
                        if(dataSnapshot.getValue().toString().matches("false")){
                            voteStatus=false;
                        }

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });







    }




    @Override
    protected void onStart() {
        super.onStart();

        //number og post View
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = databaseReference.child("Election").child(election).child("Post").child("President").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);
                        lVPresident.setText("(" + numOfPost +")");
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post").child("General Secretary").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);
                        lvGs.setText("(" + numOfPost +")");
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post").child("Join Secretary").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);

                        lvJs.setText("(" + numOfPost +")");

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post").child("Publication And Communication").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);
                        lvPaB.setText("(" + numOfPost +")");
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post").child("Treasurer").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);
                        lvTsRr.setText("(" + numOfPost +")");
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });

        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post").child("Vice President").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);
                        lvVp.setText("(" + numOfPost +")");
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post").child("Members").child("numberOfPost");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String numOfPost = dataSnapshot.getValue(String.class);
                        lvMmb.setText("(" + numOfPost +")");
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });


        //post View
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("President")) {
                                presView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("Vice President")) {
                                vPresView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("Treasurer")) {
                                tsrView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("General Secretary")) {
                                gsView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("Publication And Communication")) {
                                pabView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("Join Secretary")) {
                                jsView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                numberOfPostRef = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", numberOfPostRef.toString());
                numberOfPostRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("Members")) {
                                mmbView.setText(postModel. getPostName());
                            }

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });















        initPostData();

    }



    private void initPostData() {
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                newReference = dataRef.child("Election").child(election).child("Post");
                Log.v("Election: ", newReference.toString());
                newReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot saveDataSnapshot : dataSnapshot.getChildren()) {
                            AddCandidate.PostModel postModel = saveDataSnapshot.getValue(AddCandidate.PostModel.class);
                            if (postModel.getPostName().equals("President")) {
                                presidentModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            } else if (postModel.getPostName().equals("Vice President")) {
                                vpModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            } else if (postModel.getPostName().equals("Treasurer")) {
                                tsModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            } else if (postModel.getPostName().equals("General Secretary")) {
                                gsModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            } else if (postModel.getPostName().equals("Publication And Communication")) {
                                pcModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            } else if (postModel.getPostName().equals("Join Secretary")) {
                                jsModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            } else if (postModel.getPostName().equals("Members")) {
                                mmbModel = new AddCandidate.PostModel(postModel.getPostName(), postModel.getPostId(),postModel.getElectionId(), postModel.getNumberOfPost());
                            }
                            dataList.add(postModel);


                        }

                        SaveDataList saveDataList = new SaveDataList(BallotBox.this, dataList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }


                });







            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });


        // fetch post related data from firebase

        // call this method when posts are fetched

        initCandidateView();


    }

    private void initCandidateView() {
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("Treasurer");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candTreasurer.getChildCount() > 0) candTreasurer.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);
                            addCandidateList.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candTreasurer, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountTreasurer == Integer.parseInt(tsModel.getNumberOfPost())) {
                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();
                                    } else if (!b) {
                                        selectedTreasurer.remove(addCandidate.getCandidateName());
                                        candCountTreasurer--;
                                    } else {
                                        selectedTreasurer.add(addCandidate.getCandidateName());
                                        candCountTreasurer++;
                                    }
                                }
                            });
                            candTreasurer.addView(v);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }

                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("President");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candPresident.getChildCount() > 0) candPresident.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);
                            addCandidateList1.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candPresident, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountPresident == Integer.parseInt(presidentModel.getNumberOfPost())) {

                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();


                                    } else if (!b) {
                                        selectedPresident.remove(addCandidate.getCandidateName());
                                        candCountPresident--;
                                    } else {
                                        selectedPresident.add(addCandidate.getCandidateName());
                                        candCountPresident++;
                                    }
                                }
                            });
                            candPresident.addView(v);


                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("Vice President");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candVicePresident.getChildCount() > 0) candVicePresident.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);
                            addCandidateList2.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candVicePresident, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountVicePresident == Integer.parseInt(vpModel.getNumberOfPost())) {
                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();
                                    } else if (!b) {
                                        selectedVicePresident.remove(addCandidate.getCandidateName());
                                        candCountVicePresident--;
                                    } else {
                                        selectedVicePresident.add(addCandidate.getCandidateName());
                                        candCountVicePresident++;
                                    }
                                }
                            });
                            candVicePresident.addView(v);
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });




            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("General Secretary");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candGeneralSecretary.getChildCount() > 0) candGeneralSecretary.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);
                            addCandidateList3.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candGeneralSecretary, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountGeneralSecretary == Integer.parseInt(gsModel.getNumberOfPost())) {
                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();
                                    } else if (!b) {
                                        selectedGeneralSecretary.add(addCandidate.getCandidateName());
                                        candCountGeneralSecretary--;
                                    } else {
                                        selectedGeneralSecretary.add(addCandidate.getCandidateName());
                                        candCountGeneralSecretary++;
                                    }
                                }
                            });
                            candGeneralSecretary.addView(v);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });





            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("Join Secretary");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candJoinSecretary.getChildCount() > 0) candJoinSecretary.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);
                            addCandidateList4.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candJoinSecretary, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountJoinSecretary == Integer.parseInt(jsModel.getNumberOfPost())) {
                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();
                                    } else if (!b) {
                                        selectedJoinSecretary.remove(addCandidate.getCandidateName());
                                        candCountJoinSecretary--;
                                    } else {
                                        selectedJoinSecretary.add(addCandidate.getCandidateName());
                                        candCountJoinSecretary++;
                                    }
                                }
                            });
                            candJoinSecretary.addView(v);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });






            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("Publication And Communication");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candPublication.getChildCount() > 0) candPublication.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);

                            addCandidateList5.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candPublication, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountPublication == Integer.parseInt(pcModel.getNumberOfPost())) {
                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();
                                    } else if (!b) {
                                        selectedPublication.remove(addCandidate.getCandidateName());
                                        candCountPublication--;
                                    } else {
                                        selectedPublication.add(addCandidate.getCandidateName());
                                        candCountPublication++;
                                    }
                                }
                            });
                            candPublication.addView(v);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });







            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });
        electionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String election = "";
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Log.v("Election: ", snapshot.getKey().toString());
                    election = snapshot.getKey().toString();

                }
                databaseReference = dataRef.child("Election").child(election).child("Candidates").child("Members");
                Log.v("Election: ", databaseReference.toString());
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (candMember.getChildCount() > 0) candMember.removeAllViews();
                        for (DataSnapshot addCandidateSnapshot : dataSnapshot.getChildren()) {
                            final AddCandidate addCandidate = addCandidateSnapshot.getValue(AddCandidate.class);

                            addCandidateList6.add(addCandidate);
                            View v = LayoutInflater.from(BallotBox.this).inflate(R.layout.item_ballot_candidate, candMember, false);
                            CheckBox cb = v.findViewById(R.id.checkBox);
                            cb.setText(addCandidate.getCandidateName());

                            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (b && candCountMember == Integer.parseInt(mmbModel.getNumberOfPost())) {
                                        compoundButton.setChecked(false);
                                        Toast.makeText(BallotBox.this, "Limit exceeded!", Toast.LENGTH_SHORT).show();
                                    } else if (!b) {
                                        selectedMember.remove(addCandidate.getCandidateName());
                                        candCountMember--;
                                    } else {
                                        selectedMember.add(addCandidate.getCandidateName());
                                        candCountMember++;
                                    }
                                }
                            });
                            candMember.addView(v);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });







            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Handle possible errors.
            }
        });




    }

    @Override
    public void onClick(View view) {
        if(!voteStatus){
            if (view == btnVote) {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Election").child(lastElection[0]).child("Votes").push();
                ref.child("President").setValue(selectedPresident);
                ref.child("Vice President").setValue(selectedVicePresident);
                ref.child("General Secretary").setValue(selectedGeneralSecretary);
                ref.child("Join Secretary").setValue(selectedJoinSecretary);
                ref.child("Treasurer").setValue(selectedTreasurer);
                ref.child("Publication And Communication").setValue(selectedPublication);
                ref.child("Members").setValue(selectedMember);
                logInRef.setValue("true");

                Toast.makeText(this, "Your vote has been taken!", Toast.LENGTH_SHORT).show();
                finish();
            }

        }
        else{
            Toast.makeText(this, "you already voted!", Toast.LENGTH_SHORT).show();
        }

    }

}
