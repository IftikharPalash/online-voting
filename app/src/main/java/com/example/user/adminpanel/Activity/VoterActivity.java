package com.example.user.adminpanel.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.adminpanel.Class.AddCandidate;
import com.example.user.adminpanel.Class.HttpClient;
import com.example.user.adminpanel.Class.VoterImportClass;
import com.example.user.adminpanel.ImportVoterActivity;
import com.example.user.adminpanel.R;
import com.example.user.adminpanel.Class.VoteAdd;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class VoterActivity extends AppCompatActivity {

    private Button showVoter;
    private Button voterImport;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voter);




        voterImport=findViewById(R.id.btnImport);



        showVoter = (Button) findViewById(R.id.btnShowVoter);
        voterImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(VoterActivity.this, ImportVoterActivity.class);
                startActivity(next);
            }
        });

        showVoter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(VoterActivity.this, VoteRetrieved.class);
                startActivity(next);
            }
        });



    }








    @Override
    protected void onStart() {
        super.onStart();

    }




}
